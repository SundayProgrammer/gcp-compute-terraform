resource "google_compute_instance" "vm_instance" {
  name         = "compute-machine"
  zone         = "us-west1-a"
  machine_type = "f1-micro"
  # machine_type = "n1-highcpu-8"

  boot_disk {
    initialize_params {
      image = "projects/ubuntu-os-cloud/global/images/ubuntu-1804-bionic-v20200218"
      type  = "pd-standard"
      size  = "20"
    }
  }

  metadata_startup_script = file("./scripts/cpu_compute_init.sh")

  network_interface {
    network = "default"
    access_config {
    }
  }
}
