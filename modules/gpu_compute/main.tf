resource "google_compute_instance" "vm_instance" {
  name         = "compute-machine"
  machine_type = "n1-standard-1"

  boot_disk {
    initialize_params {
      image = "ubuntu/ubuntu-1804"
      type  = "pd-standard"
      size  = "20"
    }
  }

  metadata_startup_script = ""

  network_interface {
    network = "default"
    access_config {
    }
  }

  guest_accelerator {
    type  = "nvidia-tesla-T4"
    count = 1
  }
}
