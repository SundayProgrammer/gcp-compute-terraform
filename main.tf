provider "google" {
  credentials = file("keys/service_account_key.json")
  project     = var.project_name
  region      = "us-west1"
}

module "cpu_compute" {
  source   = "./modules/cpu_compute"
  gcp_zone = var.gcp_region
}

module "gpu_compute" {
  source = "./modules/gpu_compute" 
}
