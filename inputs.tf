variable "gcp_region" {
  default = "us-central1"
}

variable "project_name" {
  default = "project-name"
}
